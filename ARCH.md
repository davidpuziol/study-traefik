# Arquivo de Configuração

![Config](./resources/static-dynamic-configuration.webp)

Existem duas partes da configuração. Estática e dinâmica.

Existe 3 maneiras que podem definir as configurações estáticas.

- Por arquivo yaml no /etc/traefik/traefik.yaml
  - por padrão ele busca nesse caminho mas é possível passar o arquivo de configuração por argumento.

  ```yaml
  traefik --configFile=foo/bar/myconfigfile.yml
  ```

- Por argumentos passado no commando como vimos no docker compose
  - <https://doc.traefik.io/traefik/reference/static-configuration/cli/>

- Variáveis de ambiente (Não gosto muito)
  - <https://doc.traefik.io/traefik/reference/static-configuration/env/>

## Configurações Estáticas

Elementos na configuração estática estabelecem conexões com provedores e definem os pontos de entrada que o Traefik escutará (esses elementos não mudam com frequência).

### Entrypoints

<https://doc.traefik.io/traefik/routing/entrypoints/>

Os pontos de entrada que o traefik estará escutando as requisições de entrada para encaminhar a um determinado router.

![entrypoints](./resources/entrypoints.webp)

Como o Traefik é um roteador de borda, vc deve definir os redirecionamentos de porta como se fosse a entrada de um roteador.

Observe a parte de configuração abaixo, seria um redirecionamento da porta 80 para a 443 e a porta 443 liberada.

```yaml
entryPoints:
  # Redireciona toda comunicacao http para https
  web: # nome do entrypoint
    address: :80
    http:
      redirections:
        entryPoint:
          to: https
          scheme: https

  # HTTPS endpoint
  websecure: #nome do entrypoint
    address: :443
```

Se fosse passado via argumento, observe que é só chave valor

```bash
--entrypoints.web.address=:80
--entrypoints.web.http.redirections.entryPoint.to=websecure
--entrypoints.web.http.redirections.entryPoint.scheme=https
--entrypoints.websecure.address=:443
```

### Providers

Componentes existentes da infraestrutura, podendo ser orquestradores, provedores de cloud dentre outros os quais o Traefik vai interagir através de sua API (application programming interface) para encontrar informações relevantes sobre o roteamento e como o traefik irá detectar e reagir a estas mudanças. De uma forma mais simples, é através dos providers que o Traefik descobre os services e as configurações que neles existem para roteamento.

### Possíveis configurações estáticas para o Traefik

```yaml
global:
# Confere períodicamente se tem nova release. Padrão: true
  checkNewVersion: true
# Enviar dados anonimos. Padrão: true
  sendAnonymousUsage: true
serversTransport:
# Desabilite a verificação do certificado SSL Padrão: false 
  insecureSkipVerify: true
  rootCAs:
    - foobar
    - foobar
# Controla a ociosidade máxima (keep-alive) para manter por host. Padrão: 200
  maxIdleConnsPerHost: 200
  forwardingTimeouts:
# O tempo de espera até que uma conexão com um servidor de back-end possa ser estabelecida. Se zero, não existe tempo limite. (Padrão: 30)
    dialTimeout: 0
# A quantidade de tempo para aguardar os cabeçalhos de resposta de um servidor após a gravação completa da solicitação (incluindo seu corpo, se houver). Se zero, não existe tempo limite. (Padrão: 0)
    responseHeaderTimeout: 0
# O período máximo pelo qual uma conexão keep-alive HTTP ociosa permanecerá aberta antes de se fechar (Padrão: 90)
    idleConnTimeout: 90

# Defini o ponto de entrada do traefik
entryPoints:
  EntryPoint0: #nome do entrypoint
    address: foobar
    transport:
      lifeCycle:
        requestAcceptGraceTimeout: 42s
        graceTimeOut: 42s
      respondingTimeouts:
        readTimeout: 42s
        writeTimeout: 42s
        idleTimeout: 42s
    proxyProtocol:
      insecure: true
      trustedIPs:
        - foobar
        - foobar
    forwardedHeaders:
      insecure: true
      trustedIPs:
      - foobar
      - foobar
    http:
      redirections:
        entryPoint:
          to: foobar
          scheme: foobar
          permanent: true
          priority: 42
      middlewares:
        - foobar
        - foobar
      tls:
        options: foobar
        certResolver: foobar
        domains:
          - main: foobar
            sans:
              - foobar
              - foobar
          - main: foobar
            sans:
              - foobar
              - foobar
    http2:
# especifica o número de fluxos simultâneos por conexão que cada cliente tem permissão para iniciar. (Padrão: 250)
      maxConcurrentStreams: 250
    http3:
# porta UDP para anunciar, na qual HTTP/3 está disponível. (Padrão: 0)
      advertisedPort: 0
# o tempo limite define quanto tempo esperar em uma sessão ociosa antes de liberar os recursos relacionados. (Padrão: 3)
    udp:
      timeout: 3

# Os providers são as tecnologias que precisamos configurar que conterão as labels para redirecionamento, Voce só irá configurar as que vc for utilizar.

providers:
# Duração da limitação de back-ends: duração mínima entre 2 eventos de provedores antes de aplicar uma nova configuração. Evita recarregamentos desnecessários se vários eventos forem enviados em um curto período de tempo. (Padrão: 2)
  providersThrottleDuration: 2
  docker:
    #...
  file:
    #...
  marathon:
    #...
  kubernetesIngress:
    #...
  kubernetesCRD:
    #...
  kubernetesGateway:
    #...
  rest:
    #...
  rancher:
    #...
  consulCatalog:
    #...
  nomad:
    #...
  ecs:
    #...
  consul:
    #...
  etcd:
    #...
  zooKeeper:
    #...
  redis:
    #...
  http:
    #...
  plugin:
    #...

api:
# Ative a API diretamente no entryPoint chamado traefik. (Padrão: false)
  insecure: true
  # para ativar o painel
# Ativar painel. (Padrão: true)
  dashboard: true
# Habilite endpoints adicionais para depuração e criação de perfil. (Padrão:
  debug: false
  
# Para habilitar as metricas deve ser definida aqui e somente configure o que vc vai usar.
metrics:
  prometheus:
  # ...
  datadog:
  # ...
  statsD:
  # ...
  influxDB:
  # ...
  influxDB2:
  # ...

# Não é bom em produção ativar esse bloco. Geralmente um roteador responder ping para mostrar que está vivo pode gerar algum tipo de ataque.
ping:
# EntryPoint (Padrão: traefik)
  entryPoint: traefik
  manualRouting: false
# Terminando o código de status (Padrão: 503)
  terminatingStatusCode: 503

log:
# Nível de log definido para logs do traefik. (Padrão: ERROR) INFO|DEBUG|ERROR
  level: INFO
# Caminho do arquivo de log do Traefik. Stdout é usado quando omitido ou vazio.
  filePath: /tmp/traefik.
# Formato de log do Traefik: json | common (Padrão: common)
  format: json

# Se habilitar esse bloco terá acesso ao log
accessLog:
#Caminho do arquivo de log de acesso. Stdout é usado quando omitido ou vazio.
  filePath: foobar
# Pode ser common ou json
  format: json
  filters:
    statusCodes:
      - foobar
      - foobar
#Mantenha os logs de acesso quando pelo menos uma nova tentativa aconteceu. (Padrão: false)
    retryAttempts: true
# Mantenha os logs de acesso quando a solicitação demorar mais do que a duração especificada. (Padrão: 0)
    minDuration: 0
# filds é usado para substituicao de campos
  fields:
    defaultMode: foobar
    names:
      name0: foobar
      name1: foobar
    headers:
      defaultMode: foobar
      names:
        name0: foobar
        name1: foobar
# Número de linhas de log de acesso a serem processadas em buffer. (Padrão: 0)
  bufferingSize: 0

# tecnologia de tracing que irá usar
tracing:
# Defina o nome para este serviço. (Padrão: traefik)
  serviceName: foobar
# Defina o limite máximo de caracteres para nomes Span (padrão 0 = sem limite). (Padrão: 0)
  spanNameLimit: 0
  jaeger:
    #...
  zipkin:
    #...
  datadog:
    #...    
  instana:
    #...
  haystack:
    #...
  elastic:
    #...

# Se
hostResolver:
# Flag para habilitar/desabilitar o nivelamento de CNAME (Padrão: false)
  cnameFlattening: true
# resolv.conf usado para resolução de DNS (Padrão: /etc/resolv.conf)
  resolvConfig: foobar
# a profundidade máxima da resolução recursiva de DNS (padrão: 5)
  resolvDepth: 5

# Gerencia de certificados
certificatesResolvers:
  CertificateResolver0: # Nome do resolver
    acme:
# Endereço de e-mail usado para registro.
      email: foobar
      caServer: foobar
# Duração dos certificados em horas. (Padrão: 2160)
      certificatesDuration: 2160
      preferredChain: foobar
# Armazenamento para usar. (Padrão: acme.json)
      storage: acme.json
# KeyType usado para gerar a chave privada do certificado. Permitir valor 'EC256', 'EC384', 'RSA2048', 'RSA4096', 'RSA8192'. (Padrão: RSA4096)
      keyType: RSA4096
      eab:
# Identificador de chave da CA externa.
        kid: foobar
# Chave HMAC codificada em Base64 da CA externa.
        hmacEncoded: foobar
# Ative o Desafio DNS-01. (Padrão: false)
      dnsChallenge:
        provider: foobar
# Suponha que o DNS se propague após um atraso em segundos, em vez de localizar e consultar servidores de nomes. (Padrão: 0)
        delayBeforeCheck: 0
        resolvers:
          - foobar
          - foobar
# Desative as verificações de propagação de DNS antes de notificar a ACME de que o desafio de DNS está pronto. [não recomendado] (Padrão: false)
        disablePropagationCheck: false
# Se este bloco for habilitado ele vai ativar o desafio http-01 para o entrypoint abaixo
      httpChallenge:
        entryPoint: nomedoentrypoint
# Ative o Desafio TLS-ALPN-01. (Padrão: true)
      tlsChallenge: {}
  CertificateResolver1: # Nome do outro resolver
    acme:
      #...

# Para habilitar o dashboard
pilot:
  token: foobar
  dashboard: true

hub:
  tls:
    insecure: true
    ca: foobar
    cert: foobar
    key: foobar
experimental:
  kubernetesGateway: true
  http3: true
  hub: true
  plugins:
    Descriptor0:
      moduleName: foobar
      version: foobar
    Descriptor1:
      moduleName: foobar
      version: foobar
  localPlugins:
    Descriptor0:
      moduleName: foobar
    Descriptor1:
      moduleName: foobar
```

## Configurações Dinâmicas

As configurações dinâmicas contém tudo o que define como as solicitações são tratadas pelo seu sistema. Essa configuração pode mudar e é recarregada a quente sem interrupções, sem qualquer interrupção de solicitação ou perda de conexão. Essas configurações são passadas através de labels nos containers e pods e não ficam em um arquivo de configuração, mas na definição do serviço.

### Routers

<https://doc.traefik.io/traefik/routing/routers/>

Responsáveis por conectar as requisições de entradas aos serviços que irão lidar com os dados recebidos. Durante esse processo pode ser necessário manipular os dados e para isso utiliza-se os middlewares.

Conferir mais detalhadamente em [Router](./ROUTERS.md)

### Middlewares

<https://doc.traefik.io/traefik/middlewares/overview/>

![middlewares](./resources/middlewares.png)

São um meio de ajustar as solicitações antes de serem enviadas ao seu serviço (ou antes que a resposta dos serviços seja enviada aos clientes).

Existem vários middlewares disponíveis no Traefik, alguns podem modificar a requisição, os cabeçalhos, alguns são responsáveis ​​pelos redirecionamentos, alguns adicionam autenticação, e assim por diante.

<https://doc.traefik.io/traefik/middlewares/http/overview/>

Conferir alguns principais em [Middlewares](./MIDDLEWARES.md)

## Services

São responsáveis ​​por configurar como alcançar os serviços reais que eventualmente tratarão as solicitações recebidas.

### Certificados

São utilizados para criptografia TLS (Transport Layer Security) dos dados enviados para o servidor a fim de garantir uma camada de segurança, o traefik possui integração com o Let's Encrypt que utilizaremos para gerar o certificado do website.
