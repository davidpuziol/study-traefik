# Services

<https://doc.traefik.io/traefik/routing/services/>

Os services podem ser descobertos automaticamente utilizando a configuração do provider, mas podem ser definidos dinâmicamente também. Geralmente usá-se através dos [providers](./PROVIDERS.md) para ficar ainda mais automatizado.

