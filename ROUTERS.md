# Router

<https://doc.traefik.io/traefik/routing/overview/>

Router também funciona como um load balancer no Traefik. Se existir mais de um service que responde para a mesma rota automaticamente o Traefik distribui as requisições como um load balancer utilizando o algorítmo round robin.

![routers](./resources/router.webp)

Os routers analisam as solicitações que chegam nos entrypoints e passa para os middlewares, caso exista, ou passam diretamente para o service que responde nesta rota caso exista.

Uma vez que temos um entrypoint definido como esse abaixo, ou seja, ele vai escutar na porta 80.

```yaml
entryPoints:
  web:
    # Aberta a porta 80
    address: :80

providers:
# Nesta parte estamos definindo um provider para descobrir os routers baseado em files.
  file:
    directory: /path/to/dynamic/conf
```

Temos a seguinte configuração para um router. Essa configuração abaixo está em yaml, dentro do path `/path/to/dynamic/conf`.

```yaml
http:
  routers:
    # definindo a conexão para o serviço nginx
    to-nginx:
    # Analisando a requisição, se for para web.example.com irá para o service nginx, mas antes passará pelo middleware test-user

    # rules são para conferir os matchs se bate ou não.
      rule: "Host(`web.example.com`)"
    
    # se a regra bater bater passa pelo middleware e depois vai para o service, caso o restorno do middleware seja possitivo.
    # O middleware pode desviar a rota também.
      middlewares:
      - test-user
      service: nginx

  middlewares:
    # Nesse caso o middleware irá conferir autenticação baseado em usuário e senha.
    test-user:
      basicAuth:
        users:
        - test:$$apr1$$H6uskkkW$$IgXLP6ewTrSuBkTrqE8wj/

  services:
   # Aqui defini qual o service irá responder
    nginx:
      loadBalancer:
        servers:
        - url: http://private/nginx-service
```

Uma segunda maneira é definir somente o entrypoint como mostrado abaixo e colocar toda a configuração específica do serviço como labels nos containers. O provider para o docker nesse caso teria que estar definido na configuração estática do traefik.

```yaml
entryPoints:
  web-http:
    # Aberta a porta 80
    address: :80
```

Agora usando as labels na definição do service (container, pods, etc). Abaixo uma amostra como um container.

```yaml

  nginx:
    image: nginx
#    ...
# outras configuracoes do serviço
#    ...
    labels:
    # Ativando o traefik. É obrigatório para que as labels sejam vinculadas ao traefik.
      - "traefik.enable=true"
    # Qual o entrypoint ele vincular a regra
      - "traefik.http.routers.to-nginx.entrypoints=web-http"
    # O que ele espera para dar o match no entrypoint que foi acoplado
      - "traefik.http.routers.to-nginx.rule=Host(`web.example.com`)"
    # Quais middlewares irão analisar a request antes de ir par ao service. Isso poderia ser uma lista
      - "traefik.http.routers.to-nginx.middlewares=test-user"
    # Qual service irá responder
      - "traefik.http.routers.to-nginx.service=nginx"
    # Definição do middleware para esse container.
      - "traefik.http.middlewares.test-user.basicauth.users=test:$$apr1$$H6uskkkW$$IgXLP6ewTrSuBkTrqE8wj/"
    # Definição do service
    # Nesse caso está mandando container com nome de nginx foi descoberto pelo provider já definido anteriormente na configuração do traefik (o nome do container é o nome do service).
      - "traefik.http.services.nginx.loadbalancer.server.port=80"
      - "traefik.http.services.nginx.loadbalancer.server.scheme=http"
```
