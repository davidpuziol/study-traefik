# Traefik Hub

![gopher-hub](./resources/gopher-discovering.webp)

O Tunnel do traefik é criado pelo [Hub](https://traefik.io/traefik-hub/).

O Traefik Hub é uma plataforma de rede nativa da nuvem que ajuda a publicar e proteger contêineres na borda instantaneamente. O Traefik Hub fornece um gateway para seus serviços.

Vantagens:

- Publicação - (Implante o contêiner do Hub, selecione seus serviços e obtenha acesso público seguro aos seus contêineres)
- Segurança - (Acesse seus contêineres por meio de túneis seguros, implante autenticação padrão do setor e automatize o gerenciamento de certificados TLS.)
- Centralização - Deployando vários agentes em locais diferentes é possível gerênciar todas as publicações de uma única interface.

## Arquitetura

![diagrama-hub](./resources/hub-diagram.webp)

O Traefik Hub suporta multiplos secure tunnels com os seus agents, logo a escalabilidade de infra pode acontecer facilmente.

O Traefik Hub se conecta aos seus clusters por meio de um túnel privado e criptografado criado pelo agent. A utilização do túnel permite que você publique seus contêineres para acesso externo, sem se preocupar com configurações complexas e inseguras de IP público ou NAT.

Para cada serviço publicado, o Traefik Hub fornece um nome DNS exclusivo que pode ser usado imediatamente para acessar o contêiner.

O Traefik Hub solicitará, renovará e propagará certificados ACME para todos os seus clusters.

## Instalação

<https://doc.traefik.io/traefik/traefik-hub/>

No traefik que vc já tem instalado você deve habilitar essa funcinalidade no config ou passar por argumentos.

É necessário que o Traefik esteja habilitado para se comunicar com o client. Se fossemos passar os argumentos seriam esses

--experimental.hub=true
--hub.tls.insecure=true
--metrics.prometheus.addrouterslabels=true

mas poderíamos colocá-los diretamente no yaml de configuração.

```yaml
metrics:
  prometheus:
    addRoutersLabels: true

experimental:
  hub: true

hub:
  tls:
    insecure: true
```

em relação ao tls se fosse para trabalhar em produção não é indicado utilizar o insecure.

```yaml
hub:
  tls:
    ca: /path/to/ca.pem
    cert: /path/to/cert.pem
    key: /path/to/key.pem
```

Agent for criado ele gerará um token para autenticação.

Um exemplo de container agent deve ser a mesma que o traefik de fato esteja usando. No meu caso eu criei uma anteriomente chamada dockernet.

Para o entrypoint devemos passar o `run` e os argumentos abaixo

```bash
docker run -d  --volume /var/run/docker.sock:/var/run/docker.sock \
--restart="on-failure" \
--network traefik-hub \
--name=hub-agent ghcr.io/traefik/hub-agent-traefik:v0.7.2 \
run \
--auth-server.advertise-url=http://hub-agent \
--hub.token=27254938-6490-42b6-95c4-95c9b286df1b \
--traefik.host=traefik \
--traefik.tls.insecure=true
```

assim que estivermos com o agent rodando irá mosstrar o status online e todos os services do docker que ele capturou.

![agent](./resources/agentok.png)a

## Acess Control

Para criar uma restrição de acesso somente crie em

![policy](./resources/acesspolicy.png)

![policycreate](./resources/accesspolicycreate.png)

e logo após isso quando for publicar um serviço terá a possibilidade poderá escolher essa politica.

## Publish

Para publicar um serviço é simples

![policy](./resources/publish.png)

Colocando a policy caso queira

![policy](./resources/publishcreate.png)

![policy](./resources/deployservicehub.png)

![policy](./resources/hubdns.png)

![policy](./resources/traefichubservicerpublish.png)

A partir de agora esse link dá acesso ao serviço.
