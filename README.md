# Study Traefik

![Traefik](./resources/traefik.png)

[Documentação Oficial](https://doc.traefik.io/traefik/)

O traefik pode atuar como:

- proxy reverso
- balanceador de carga
- api gateway
- kubernetes ingress
- gerenciador de certificados

É open source e desenvolvido em golang por isso usa o mascote do golang.
[Repo do Projeto](https://github.com/traefik/traefik)

Esta na [CNCF](https://landscape.cncf.io/card-mode?grouping=no&sort=stars&selected=traefik) e já nasceu pensando para a cloud e microserviços.

Ele descobre automaticamente qual serviço irá responder a solicitação baseado em labels (rotulos). Não é fazer ajustes de regras no load balancer a cada serviço que for deployado pois ele trabalha dinamicamente. Se vc definir que um serviço X deve ser respondido por todos os containers com uma determinada label X, o balanceamento de carga é feito automaticamente.

![ProxyAndLB](./resources/proxyandloadbalancer.png)

Compatível com as principais tecnologias de orquestradores, mas também pode atuar de forma simples sem uso de orquestradores como no caso de um proxy reverso simples para http/https.

![ManyTechs](./resources/traefik-architecture.webp)

Além disso é capaz de trabalhar com path e subdomínios, para filtrar a requisição.

## Requisitos

Para que o traefik funcione é necessário ter um container runtime instalado em todos os nodes que ele irá atuar.

Pode ser utilizado o Containerd (utilizado pelo docker) ou até mesmo o Podman.

Para que a mágica ocorra é necessário que o Traefik escute (read only) o sock do container runtime que vc estiver usando. Logo, é necessário que o Traefik esteja em todos os nodes de um cluster como um DaemonSet no Kubernetes ou Global no Swarm.

Para instalação do docker <https://docs.docker.com/get-docker/>

Existe um script rápido de instalação do docker mas não deve ser usado em produção inclusive já vem com a instalação do docker compose no script

```bash
curl -fsSL https://get.docker.com | sudo bash
```

**Demonstração rápida** da parte que cabe ao Traefik dentro de um docker compose.

```yaml
  traefik:
    image: "traefik"
    ## toda essa parte poderia ir para um arquivo de configuração
    #############################################################
    command:
      - --entrypoints.web.address=:80
      - --entrypoints.websecure.address=:443
      - --providers.docker
      - --api
      - --certificatesresolvers.leresolver.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory
      - --certificatesresolvers.leresolver.acme.email=your@email.com
      - --certificatesresolvers.leresolver.acme.storage=/acme.json
      - --certificatesresolvers.leresolver.acme.tlschallenge=true
      
    #############################################################
    ports:
      - "80:80"
      - "443:443"
    volumes:
      # no caso do docker esse é o container runtime sock
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
      - "./acme.json:/acme.json"
      
      # Para usar essa hora do sistema para gravar os logs, caso contrário vai gravar na UTC
    environment:
      - TZ=America/Sao_Paulo
    
    98
    labels:
      # Dashboard
      - "traefik.http.routers.traefik.rule=Host(`dashboard.dominio.com`)"
      - "traefik.http.routers.traefik.service=api@internal"
      - "traefik.http.routers.traefik.tls.certresolver=letsencrypt"
      - "traefik.http.routers.traefik.entrypoints=websecure"
      - "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)"
      - "traefik.http.routers.http-catchall.entrypoints=web"
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"
      # Para já definir um password no traefik é necessário passar o usuario e a senha em hash
      - "traefik.http.routers.traefik.middlewares=authtraefik"
      - "traefik.http.middlewares.authtraefik.basicauth.david:$$2y$$05$$zmvXMaluny/RJxdfvYuQ/OQGLrFeNwymnl5NJKqLcNSlPbm6xXdAe"
    #############################################################
```

Para gerar a senha pode online mesmo pode usar o site <https://www.devglan.com/online-tools/bcrypt-hash-generator>

![SenhaHash](./resources/hashgeneratorpassword.png)

Porém uma limitação do yaml é o uso do caractere $. É necessário dobrar esse caractere para fazer o scape.

```yaml
$2a$04$.u02kTQjf2B3IUT5YDEtPu2RdQLtXWSSjJPLcGaSTcQJQWCYAJY2y
#deve ser
$$2a$$04$$.u02kTQjf2B3IUT5YDEtPu2RdQLtXWSSjJPLcGaSTcQJQWCYAJY2y
```

Essa senha em hash pode ser criada utilizando o htpasswd do pacote apache2-utils

```bash
apt-get install apache2-utils
echo $(htpasswd -nbB david "senhadeteste") | sed -e s/\\$/\\$\\$/g
```

Agora a parte do service com as labels definidas para serem gerenciados pelo traefik

```yaml
  webapp:
    image: containous/whoami:v1.3.0
    labels:
    # qual o subdominio que ele responderá
      - "traefik.http.routers.app1.rule=Host(`web.dominio.com`)"
    # se deve mandar http ou https a requisicao para o serviço
      - "traefik.http.routers.app1.entrypoints=websecure"
      - "traefik.http.routers.app1.tls=true"
    # e quem é a autoridade certificado que resolve o certificado
      - "traefik.http.routers.app1.tls.certresolver=letsencrypt"
```

De uma conferida em [quick start](./QUICKSTART.md) para criar um rapidamente de forma muito simples.

## Arquitetura e Configuração

Para entender melhor o Traefik vamos analisar a [Arquitetura](./ARCH.md)

## Instalação

Para configurar instalação padrão do traefik [Instalação](./ARCH.md)
