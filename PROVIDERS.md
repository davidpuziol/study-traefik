# Providers

Varios providers estão disponíveis na documentação, mas o foco desse estudo é docker e kubernetes então vou me atentar somente aos dois de acordo com a evolução do conhecimento.

A configuração do provider faz com que o traefik descubra **automágicamente** os services.

Quando um service é descoberto pelo provider, usa-se nas labels `traefik.http.services.xxxx`

## Docker

Configuração para o docker no arquivo `/etc/traefik/traefik.yaml`.

```yaml

  docker:
  # valor default par ao docker.sock
    endpoint: "unix:///var/run/docker.sock"
    watch: true
  # qual a network ele irá encontrar os containers, se omitir irá buscar em todas as networks existintes
    network: dockernet
    # Default host rule to containername.domain.example
    defaultRule: "Host(`{{ lower (trimPrefix `/` .Name )}}.puziol.com.br`)"    # Replace with your domain
    
    # Esta opção defini que todos os containers iniciarão com traefik.enable = false , mas pode ser mudada container a containers.
    exposedByDefault: false
 
    swarmMode: false  # O padrão é falso
    swarmModeRefreshSeconds: 15 # o padrão é 15
```

Todo serviço que é descoberto automaticamente já é criado um route para ele em todos os entrypoints definidos estaticamente.

