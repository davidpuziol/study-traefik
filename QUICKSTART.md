# Configuração Rapida

<https://doc.traefik.io/traefik/getting-started/quick-start/>

Seguindo esse tutorial vamos rapidamente subir um traefik com um docker compose

```yaml
version: '3'

services:
  reverse-proxy:
    # The official v2 Traefik docker image
    image: traefik:v2.8
    # Enables the web UI and tells Traefik to listen to docker
    command: --api.insecure=true --providers.docker
    ports:
      # The HTTP port
      - "80:80"
      # The Web UI (enabled by --api.insecure=true)
      - "8080:8080"
    volumes:
      # So that Traefik can listen to the Docker events
      - /var/run/docker.sock:/var/run/docker.sock
```

```bash
docker-compose up -d --scale whoami=5
```

Entradando no dashboard podemos conferir serviços que ele encontrou pelo docker.sock
![service](./resources/traefikservices.png)

Entrando no nosso serviço podemos ver que ele já tem o endereço de todos os containers que respondem essa solicitação

![servicescaled](./resources/traefikservicesscaled.png)
