# Middlewares

<https://doc.traefik.io/traefik/middlewares/overview/>

Atua sob a request ou response durante o roteamento para o serviço desejado.

Podem influenciar sob:

- http <https://doc.traefik.io/traefik/middlewares/http/overview/>
- tcp <https://doc.traefik.io/traefik/middlewares/tcp/overview/>

Abaixo alguns dos mais usados, porém vale conferir as páginas acima conferir o que temos a mais.

## BasicAuth (http)

É um middleware para restringir acesso somente a usuários conhecidos.

![basicauth](./resources/basicauth.webp)

Uma label como essa por essa somente permitirá o usuário test com a senha que bate com esse hash continuar, senão vai receber acesso negado.

>"traefik.http.middlewares.test-auth.basicauth.users=`test`:`$$apr1$$H6uskkkW$$IgXLP6ewTrSuBkTrqE8wj/,test2:$$apr1$$d9hr9HBB$$4HxwgUir3HP4EsggP/QNo0`"

Um user pode ter 2 senhas? Sim. Observe que usamos o mesmo usuário no exemplo abaixo.

"traefik.http.middlewares.test-auth.basicauth.users=`test`:`$$apr1$$H6uskkkW$$IgXLP6ewTrSuBkTrqE8wj/`,`test2`:`$$apr1$$d9hr9HBB$$4HxwgUir3HP4EsggP/QNo0`"

Assim como poderíamos passar um patch para um arquivos de users e senhas hash.

"traefik.http.middlewares.test-auth.basicauth.usersfile=`/path/to/my/usersfile`"

```file
test:$apr1$H6uskkkW$IgXLP6ewTrSuBkTrqE8wj/
test2:$apr1$d9hr9HBB$4HxwgUir3HP4EsggP/QNo0
```

E ainda poderíamos declarar em um arquivo de configuração

```yaml
http:
  middlewares:
    test-auth:
      basicAuth:
        users:
          - "test:$apr1$H6uskkkW$IgXLP6ewTrSuBkTrqE8wj/"
          - "test2:$apr1$d9hr9HBB$4HxwgUir3HP4EsggP/QNo0"
```

## ForwardAuth

<https://doc.traefik.io/traefik/middlewares/http/forwardauth/>

![forwardauth](./resources/authforward.png)

Seguindo o mesmo esquema do basic auth mas com um serviço externo para verificar o usuário. Se o serviço responder com um código 2XX, o acesso é concedido e a solicitação original é realizada. Caso contrário, a resposta do servidor de autenticação é retornada.

Exemplo com uso do authelia

```yaml
  middlewares:
    auth:
      forwardauth:
        # Endereço do serviço
        address: http://authelia:9091/api/verify?rd=https://auth.dominio.com/
        # Defina a trustForwardHeaderopção true para confiar em todos os X-Forwarded-*Headers.
        trustForwardHeader: true
        # opção é a lista de cabeçalhos para copiar da resposta do servidor de autenticação e definir na solicitação encaminhada, substituindo quaisquer cabeçalhos conflitantes existentes.
        authResponseHeaders:
          - Remote-User
          - Remote-Groups
          - Remote-Name
          - Remote-Email
```

```yaml
labels:
  - "traefik.http.middlewares.test-auth.forwardauth.address=https://example.com/auth"
  - "traefik.http.middlewares.test-auth.forwardauth.trustForwardHeader=true"
  - "traefik.http.middlewares.test-auth.forwardauth.authResponseHeaders=X-Auth-User, X-Secret"
```

Ainda é possível passar o certificado para uso do tls. Vale a pena uma conferida mais detalhada nesse middleware.

## CircuitBreaker

Assim como um load balancer normal é necessário saber se o serviço está saudável para receber requisições.

![healthcheck](./resources/circuitbreaker.webp)

O CircuitBreaker protege seu sistema de empilhar solicitações a serviços não saudáveis, resultando em falhas em cascata.

Quando seu sistema está saudável, o circuito é fechado (operações normais). Quando está ruim, o circuito é aberto e as solicitações não são mais encaminhadas, mas sim tratadas por um mecanismo de fallback.

Para avaliar se o seu sistema está íntegro, o disjuntor monitora constantemente os serviços.

```yaml
labels:
  - "traefik.http.middlewares.latency-check.circuitbreaker.expression=LatencyAtQuantileMS(50.0) > 100"
```

Existem três estados possíveis:

- Fechado (seu serviço funciona normalmente)
- Aberto (o mecanismo de fallback assume seu serviço)
- Recuperando (o disjuntor tenta retomar a operação normal enviando progressivamente solicitações ao seu serviço)

A expression pode ser:

- LatencyAtQuantileMS (abre o circuito se a latência estiver ruim). `LatencyAtQuantileMS(50.0) > 100` abre o disjuntor quando a latência mediana (quantil 50) atinge 100ms.

- ResponseCodeRatio (abre o circuito baseado na taxa média do código de resposta). `ResponseCodeRatio(500, 600, 0, 600) > 0.25` abre o circuito se 25% das requisições retornarem um status 5XX (entre as requisições que retornaram um código de status de 0 a 5XX).

## Rate Limite

<https://doc.traefik.io/traefik/middlewares/http/ratelimit/>

Para controlar o número de solicitações que vão para um serviço por segundo.

```yaml
labels:
  - "traefik.http.middlewares.test-ratelimit.ratelimit.average=100"
  - "traefik.http.middlewares.test-ratelimit.ratelimit.burst=50"
```

`average` é a taxa máxima em segundo permitida para uma determinada fonte. O padrão é zero, não tem limite.

`burst` é o número máximo de solicitações permitidas no mesmo período de tempo arbitrariamente pequeno. O padrão é 1.

Várias requisições podem vir do mesmo ip, mas não ser geradas pelo mesmo cliente, pois estão atrás de uma nat por de exemplo. Confira a documentação.

## Buffering

![buffering](./resources/buffering.png)

Limita o tamanho da mensagem enviada aos serviços. Exemplo de 2MB de mensagem.

```yaml
labels:
  - "traefik.http.middlewares.limit.buffering.maxRequestBodyBytes=2000000"
```

Se a resposta do serviçoe estiver acima de 2MB o client recebe 500 de resposta.

```yaml
labels:
  - "traefik.http.middlewares.limit.buffering.maxResponseBodyBytes=2000000"
```

## Retry

O Retry reemite solicitações um determinado número de vezes para um servidor de back-end se esse servidor não responder. Assim que o servidor responde, o middleware para de tentar novamente, independentemente do status da resposta. O middleware Retry tem uma configuração opcional para habilitar uma retirada exponencial.
Serve para repetir a transação caso não tenha resposta

```yaml
labels:
  - "traefik.http.middlewares.test-retry.retry.attempts=4"
  - "traefik.http.middlewares.test-retry.retry.initialinterval=100ms"
```

A attempts opção define quantas vezes a solicitação deve ser repetida.

A `initialInterval` opção define o primeiro tempo de espera. O intervalo da próxima espera é calculado como o dobro do intervalo anterior.

## InFlightConn (tcp)

Para limitar o numero de conexões simultâneas e evitar sobrecarga do serviço.

```yaml
labels:
  - "traefik.tcp.middlewares.test-inflightconn.inflightconn.amount=10"
```

`amount` define a quantidade máxima de conexões simultâneas permitidas. O middleware fecha a conexão se já houver amount conexões abertas.

## IPWhiteList

IPWhitelist aceita ou recusa conexões baseado no ip ou range

```yaml
labels:
  - "traefik.tcp.middlewares.test-ipwhitelist.ipwhitelist.sourcerange=127.0.0.1/32, 192.168.1.7"
```

```yaml
tcp:
  middlewares:
    test-ipwhitelist:
      ipWhiteList:
        sourceRange:
          - "127.0.0.1/32"
          - "192.168.1.7"
```
