# Instalação

<https://doc.traefik.io/traefik/getting-started/install-traefik/>

## Em um container docker

Neste exemplo abaixo todos os parâmetros estarão em no arquivo traefik.yaml e este é passado para /etc/traefik/traefik.yaml dentro do container.

crie um o arquivo traefik.yaml com os parâmetros para habilitar os entrypoints.

```yaml
global:
  checkNewVersion: true
  sendAnonymousUsage: true
entryPoints:
  web:
    address: :80

  websecure:
    address: :443
```

```bash
docker run -d -p 8080:8080 -p 80:80 \
    -v $PWD/traefik.yml:/etc/traefik/traefik.yml traefik:v2.8
```

## Em um cluster kubernetes

Podemos inclusive utilizar o helm como ingress no kubernetes

<https://github.com/traefik/traefik-helm-chart>

Podemos fazer a instalação utilizando o helm.

```bash
helm repo add traefik https://helm.traefik.io/traefik
helm repo update
helm install traefik traefik/traefik
```

para expor o painel do traefik que não é feito por padrão.

```bash
kubectl port-forward $(kubectl get pods --selector "app.kubernetes.io/name=traefikv2" --output=name) 9000:9000
```
